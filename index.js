let posts = []
let count = 1 // incremental ID

const showPosts = (posts) => {
	let post_entries = ''

	posts.forEach((post) => {
		post_entries += `
			 <div id="post-${post.id}">
			     <h3 id="post-title-${post.id}">${post.title}</h3>
			     <p id="post-body-${post.id}">${post.body}</p>
			     <button onclick="editPost('${post.id}')">Edit</button>
			     <button onclick="deletePost('${post.id}')">Delete</button>
			 </div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = post_entries
}


//responsible for adding a post

document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault()
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value

	})
	// for incrementing the id to be unique for each new post
	count++

	// for showing the updated posts after adding a new post
	showPosts(posts)

	alert('Successfully added a new Post!')
})


const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault();
    // loop through the array to find the match

    for (let i = 0; i < posts.length; i++) {
        // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.


        // if match has been found. the proceed with assigning the new values to the existing post
        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;
    		//shows the updated posts once this specific post has been updated
            
            showPosts(posts);
            alert('Successfully updated.');
            
            break;
        }
    }
});


// deleting a post
const deletePost = (id) => {
	posts = posts.filter((post) => {
		if(post.id.toString() !== id) {
			return post;
		}
	});
	document.querySelector(`#post-${id}`).remove();
	alert(`data has been deleted`)
}